package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.Login;

public class TC001 extends ProjectMethods {

	
	@BeforeTest
	public void setData()
	{
		testCaseName="TC001";
		testDescription="Login";
		testNodes="acme";
		author="Subha";
		category="sanity";
		dataSheetName="TC001";
		
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password, String id)
	{
		new Login()
		.enterEmail(username)
		.enterPassword(password)
		.clickLogin()
		.MOVendor()
		.ClickSearch()
		.enterVendorID(id)
		.clickSearch()
		.verifyText();
	}
}
