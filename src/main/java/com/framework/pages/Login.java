package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class Login extends ProjectMethods {
	
	
	public Login() 
	   
	   {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
        }
	
	@FindBy(how = How.ID,using="email") WebElement eleEmail;
	public Login enterEmail(String username){
		clearAndType(eleEmail, username);
		return this;
	}	
	
	@FindBy(how = How.ID,using="password") WebElement elePwd;
	public Login enterPassword(String password){
		clearAndType(elePwd, password);
		return this;
	}	
	
	@FindBy(how = How.ID,using="buttonLogin") WebElement eleLog;
	public Dashboard clickLogin(){
		click(eleLog);
		return new Dashboard();
	}	


	
	
	
	
	
	

}
