package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class SearchVendor extends ProjectMethods {
	
	public SearchVendor() 
	   
	    {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
		}
	
@FindBy(how = How.CLASS_NAME,using="form-control") WebElement eleType;
   public SearchVendor enterVendorID(String data)
   {
	   clearAndType(eleType, data);
	   // driver.findElementByClassName("form-control").sendKeys("DE456232");
       return this;
   }
   
   @FindBy(how = How.ID, using="buttonSearch") WebElement eleClick;
   public SearchResult clickSearch()
   {
	   click(eleClick);
	   // driver.findElementById("buttonSearch").click();
	   return new SearchResult();
   }

}
